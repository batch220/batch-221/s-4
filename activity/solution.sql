
#1-a
SELECT * FROM artists WHERE name LIKE "%d%";

#1-b
SELECT * FROM songs WHERE length < 230;

#1-c
SELECT song_name, length FROM songs 
	JOIN albums ON songs.album_id = albums.id;

#1-d
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id;
	SELECT * FROM artists WHERE name LIKE "%a%";
#1-e
SELECT * FROM albums ORDER BY album_title DESC;

#1-f
SELECT * FROM albums 
	JOIN songs ON albums.id = songs.album_id;
	SELECT * FROM albums ORDER BY album_title DESC;